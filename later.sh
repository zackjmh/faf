#!/bin/bash
shopt -s globstar
g++-8 -c -Ibuild/include -I/home/arkos/.local/share/FAF/include src/lib/**/*.cpp -std=c++2a -lpthread -lstdc++fs -O3
g++-8 -Ibuild/include -I/home/arkos/.local/share/FAF/include src/bin/Drivers/FAFbuild.cpp *.o -std=c++2a /home/arkos/.local/share/FAF/FAFcore/libFAFcore.a -otest -lpthread -lstdc++fs -O3
