#pragma once

#include <string>
#include <unordered_set>

#include <FAFcore/Logger.h>
#include <FAFcore/Process.h>
#include <FAFcore/File.h>


namespace FAF::Build
{
  class FAFfile;
  class Compiler
  {
  public:
    Compiler(const std::string_view& compilerExe)
      : m_compiler{std::string{compilerExe}}
    { 
      m_flags.emplace("-std=c++2a");
      Logging::ColorLog("Using compiler %s", Logging::Colors::MAGENTA, m_compiler);
    };

    Compiler()
    {
      m_flags.emplace("-std=c++2a");
    }

    void AddIncludeDir(const Filesystem::File& dir);

    void AddStaticLib(const Filesystem::File& lib);
    
    void AddLinkerDepends(const std::string_view& depend);

    void AddFlag(const std::string_view& flag)
    {
      m_flags.emplace(flag);
    }

    void SetBuildDir(const std::string_view& buildDir)
    {
      m_buildDir = buildDir;
    }

    Filesystem::File NewestLibrary() const;

    Filesystem::File Compile(const Filesystem::File& sourceFile, const Filesystem::File& newest, const FAFfile& configFile, bool binary = false) const;

  private:

    std::string_view m_buildDir{};


    std::string m_compiler{};
    std::unordered_set<Filesystem::File, Filesystem::File::Hashor> m_includeDirs{};
    std::unordered_set<Filesystem::File, Filesystem::File::Hashor> m_staticLibs{};
    std::unordered_set<std::string> m_linkedLibs{};
    std::unordered_set<std::string_view> m_flags{};

    void StateDump(const FAFfile& configFile) const;
  };
}
