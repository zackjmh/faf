#include <FAFcore/Directory.h>

#include <FAFbuild/Compiler.h>
#include <FAFbuild/FAFfile.h>

namespace FAF::Build
{

  Filesystem::File Compiler::NewestLibrary() const
  {
    Filesystem::File newest;
    for(auto&& lib : m_staticLibs)
    {
      newest = lib.Timestamp() > newest.Timestamp() ? lib : newest;
    }

    return newest;
  }

  void Compiler::AddIncludeDir(const Filesystem::File& dir)
  {
    m_includeDirs.emplace(dir);
  }

  void Compiler::AddStaticLib(const Filesystem::File& lib)
  {
    m_staticLibs.emplace(lib);
  }

  void Compiler::AddLinkerDepends(const std::string_view& depend)
  {
    m_linkedLibs.emplace(depend);
  }

  void Compiler::StateDump(const FAFfile& configFile) const
  {
    Logging::ErrorLog("DUMPING COMPILER BUILD STATE\n");
    Logging::ErrorLog("\tINCLUDE DIRS", Logging::Colors::MAGENTA);

    static auto LogState = [](auto&& state)
    {
      Logging::ErrorLog("\t\t- %s", Logging::Colors::CYAN, state);
    };

    for(auto&& dir : m_includeDirs)
    {
      LogState(dir.Path());
    }

    Logging::ErrorLog("\n\tCONFIGURED STATIC LIBS", Logging::Colors::MAGENTA);

    for(auto&& lib : configFile.ExternalLibs())
    {
      LogState(lib);
    }
      
    Logging::ErrorLog("\n\tSTATIC LIBS", Logging::Colors::MAGENTA);

    for(auto&& lib : m_staticLibs)
    {
      LogState(lib.Path());
    }
    Logging::ErrorLog("\n\tFLAGS", Logging::Colors::MAGENTA);

    for(auto&& flag : m_flags)
    {
	    LogState(flag);
    }

    Logging::ErrorLog("\n\tCONFIGURED FLAGS", Logging::Colors::MAGENTA);

    for(auto&& flag : configFile.Flags())
    {
      LogState(flag);
    }


    Logging::ErrorLog("\n\tLINKED LIBS", Logging::Colors::MAGENTA);

    for(auto&& lib : m_linkedLibs)
    {
      LogState(lib);
    }
  }

  Filesystem::File Compiler::Compile(const Filesystem::File& sourceFile, const Filesystem::File& newest, const FAFfile& configFile, bool binary) const
  {
    const auto ext = binary ? "" : ".o"; //No Ext for executable files
    const auto locatedAt = binary ? configFile.LocalDriversDir() : configFile.LocalArchiveDir();
    const auto objectFile = sourceFile.WithExt(ext).LocatedAt(locatedAt);

    const auto newestDependency = [this](const auto& other)
    {
      const auto newestLib = this->NewestLibrary();
      return other.Timestamp() < newestLib.Timestamp() ? newestLib : other;
    }(newest.Timestamp() > sourceFile.Timestamp() ? newest : sourceFile);

    newestDependency.DoIfNewer(objectFile, [this, &objectFile, binary, &sourceFile, &configFile]()
    {
      Logging::ColorLog("Building file %s", Logging::Colors::MAGENTA, objectFile.Fullname());
       
      Collections::Vector<std::string> args{};

      if(!binary)
      {
        args.emplace_back("-c"); 
      }

      args.emplace_back("-I" + configFile.LocalIncludeDir().Path());
      
      args.ApplyAndAppend(m_includeDirs, [](auto&& dir) { return "-I" + dir.Path(); });

      args.emplace_back(sourceFile.Path());

      args.ApplyAndAppend(m_staticLibs, [](auto&& lib) { return lib.Path(); });

      args.Append(configFile.ExternalLibs());

      args.Append(m_flags);
     
      args.Append(configFile.Flags());

      args.Append(m_linkedLibs);

      args.emplace_back("-o" + objectFile.Path());

      auto compiler = System::Process(std::string{m_compiler}, args.AsVector());

      if(compiler.ReturnValue() != 0)
      {
        StateDump(configFile);

        std::string passedFlags{};

        for(auto&& arg : args)
        {
          passedFlags += arg + "\n";
        }

        Logging::ErrorLog("%s", passedFlags);

        compiler.LogErrors();

        throw std::runtime_error("Failed to compile " + sourceFile.Fullname());
      }
    }).ElseIf(Functional::Identor(true), [&objectFile]()
    {
        Logging::ColorLog("%s current, no need to recompile!", Logging::Colors::GREEN, objectFile.Fullname());
    });

    return objectFile;
 
  }
}
