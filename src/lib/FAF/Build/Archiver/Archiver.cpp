#include <FAFcore/Vector.h>
#include "Archiver.h"

namespace FAF::Build
{
  const Filesystem::File& Archiver::Archive(const Filesystem::File& objectFile) const
  {
    if(!objectFile.Exists())
    {
      throw std::runtime_error("Trying to call archive on an object file that doesn't exist!");
    }

    if(objectFile.Timestamp() > m_startTime)
    {
      Logging::ColorLog("Adding %s to archive %s", Logging::Colors::CYAN, objectFile.Fullname(), m_archive.Fullname());

      Collections::Vector<std::string> args{};

      args.emplace_back("rs");

      args.emplace_back(m_archive.Path());

      args.emplace_back(objectFile.Path());

      auto addArchive = System::Process("ar", args.AsVector());

      if(addArchive.ReturnValue() != 0)
      {
        addArchive.LogErrors();
        throw std::runtime_error("Failed to add object to archive");
      }
    }
    else
    {
      Logging::ColorLog("Archive %s newer than object file %s", Logging::Colors::GREEN, m_archive.Fullname(), objectFile.Fullname());
    }

    return objectFile;
  }
}
