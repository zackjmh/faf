#pragma once
#include <FAFcore/Logger.h>
#include <FAFcore/File.h>
#include <FAFcore/Process.h>


namespace FAF::Build
{
  
  class Archiver
  {
  public:
    Archiver(const Filesystem::File& archive) :
      m_archive{archive},
      m_startTime{m_archive.Timestamp()}
    {}
 
    const Filesystem::File& Archive(const Filesystem::File& objectFile) const;

  private:
    const Filesystem::File& m_archive;
    const std::filesystem::file_time_type m_startTime{};
  };

}
