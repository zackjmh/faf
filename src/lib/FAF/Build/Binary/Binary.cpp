#include <FAFbuild/Static.h>
#include <FAFbuild/FAFfile.h>
#include <FAFcore/Process.h>
#include <FAFcore/ParallelFor.h>

#include <FAFbuild/Binary.h>

namespace FAF::Build
{
  Binary Binary::Build(const Filesystem::ExistingDirectory& binDir, const std::vector<StaticLibrary>& depends, const FAFfile& configFile)
  {
    Binary newBin{};
  
    newBin.m_catagory = binDir.AsFile().Fullname();

    Logging::ColorLog("Building catagory %s", Logging::Colors::GREEN, newBin.m_catagory);
     
    for(auto [sourceExt, compiler] : configFile.Compiler())
    {
      for(auto&& lib : depends)
      {
        compiler.AddStaticLib(lib.Library());
      }

      const auto drivers = binDir.CollectFilesByExt(sourceExt);

      Async::ParallelDispatcher<Filesystem::File>{drivers, Functional::OnThis(&Compiler::Compile, compiler, Filesystem::File{}, configFile, true)}
        .Wait("Failed to build driver: %s", Functional::OnThis(&Collections::Vector<Filesystem::File>::EmplaceBack<const Filesystem::File&>, newBin.m_binaries));

      Logging::ColorLog("Done building catagory %s", Logging::Colors::GREEN, newBin.m_catagory);
    }
    return newBin;
  }


  std::string Binary::Catagory() const
  {
    return m_catagory;
  }
}
