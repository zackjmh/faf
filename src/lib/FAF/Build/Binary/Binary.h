#pragma once

#include <FAFcore/Directory.h>
#include <FAFcore/File.h>
#include <FAFcore/Vector.h>

#include <FAFbuild/Static.h>
#include <FAFbuild/FAFfile.h>

namespace FAF::Build
{

  class Binary
  {
  public:
    static Binary Build(const Filesystem::ExistingDirectory& libDir, const std::vector<StaticLibrary>& depends, const FAFfile& configFile);
    std::string Catagory() const;

    ~Binary(){}

    const Collections::Vector<Filesystem::File> Binaries() const
    {
      return m_binaries;
    }

    Binary() = default;
  private:

    std::string m_catagory{};
    Collections::Vector<Filesystem::File> m_binaries{};
  };
}
