#pragma once

#include <string>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <functional>
#include <FAFcore/ConfigFile.h>
#include <FAFcore/Logger.h>
#include <FAFcore/File.h>
#include <FAFcore/Directory.h>
#include <FAFcore/RequiredDirectory.h>
#include <FAFbuild/Compiler.h>


namespace FAF::Build
{
  using CompilerMap_t = std::unordered_map<std::string, Compiler>;
  using InterfaceExtMap = std::unordered_map<std::string, std::string>;

  CONFIG_FILE(FAFfile)
    NEW_KEYMAPPED_VAR(Compiler, CompilerMap_t{})
    {
      auto [extStr, compiler] = Split(inVal, ':');

      auto exts = Tokenize(extStr, ',');

      for(auto&& ext : exts)
      {
        if(!value.count(ext))
        {
          value[ext] = compiler;
        }
        else
        {
          Logging::ErrorLog("Ignoring already mapped ext %s", ext);
        }
      }
    }
    END_KEYMAPPED_VAR(Compiler)

    NEW_KEYMAPPED_VAR(InterfaceExts, InterfaceExtMap{})
    {
      const auto [sourceExtStr, interfaceExt] = Split(inVal, ':');
      const auto sourceExts = Tokenize(sourceExtStr, ',');
      
      for(const auto& sourceExt : sourceExts)
      {
        if(!value.count(sourceExt))
        {
          value[sourceExt] = interfaceExt;
        }
        else
        {
          Logging::ErrorLog("Source ext %s already mapped to interface ext %s, ignoring", sourceExt, value[sourceExt]);
        }
      }
    }
    END_KEYMAPPED_VAR(InterfaceExts)

    KEYMAPPED_VAR(ProjectName, "DefaultProject")

    KEYMAPPED_VAR(LibraryPrefix, "lib")

    KEYMAPPED_VAR(LibrarySuffix, "")

    KEYMAPPED_VAR(LibraryExt, ".a")

    MAGIC_VALUE(LibDir, std::string{"lib/"})

    DERIVED_VALUE(LibraryDir, Filesystem::RequiredDirectory{})
    {
      cache = My.BuildRoot() + My.LibDir();
    }
    END_CACHED_GET(LibraryDir)

    KEYMAPPED_VAR(CompilerFlagToken, "-")

    KEYMAPPED_VAR(CompilerLinkerPrefix, "l")

    DERIVED_VALUE(ArchiveName, std::string{})
    {    
      cache = My.LibraryPrefix() + My.ProjectName() + My.LibrarySuffix();
    }
    END_CACHED_GET(ArchiveName)

    DERIVED_VALUE(ArchiveFile, Filesystem::File{})
    {
      cache = Filesystem::File{My.ArchiveName()}.WithExt(My.LibraryExt()).LocatedAt(My.LibraryDir());
    }
    END_CACHED_GET(ArchiveFile)

    MAGIC_VALUE(BuildRoot, std::string{"build/"})
    
    MAGIC_VALUE(ArchiveDir, std::string{"archive/"})

    DERIVED_VALUE(LocalArchiveDir, Filesystem::RequiredDirectory{})
    {
      cache = My.BuildRoot() + My.ArchiveDir();
    }
    END_CACHED_GET(LocalArchiveDir)

    MAGIC_VALUE(IncludeDir, std::string{"include/"})

    DERIVED_VALUE(LocalIncludeDir, Filesystem::RequiredDirectory{})
    {  
      cache = My.BuildRoot() + My.IncludeDir();
    }
    END_CACHED_GET(LocalIncludeDir)

    MAGIC_VALUE(DriversDir, std::string{"bin/"});

    DERIVED_VALUE(LocalDriversDir, Filesystem::RequiredDirectory{})
    {
      cache = My.BuildRoot() + My.DriversDir();
    }
    END_CACHED_GET(LocalDriversDir)

    DERIVED_VALUE(HeaderPrefixDir, Filesystem::RequiredDirectory{}) 
    {
      cache = My.LocalIncludeDir().Path() + My.ProjectName() + "/";
    }
    END_CACHED_GET(HeaderPrefixDir)
    
    NEW_KEYMAPPED_VAR(Flags, std::unordered_set<std::string>{}) 
    {
        const auto flags = Tokenize(inVal, ',');
        for(const auto& flag : flags)
        {
          value.emplace(My.CompilerFlagToken() + flag);
        }
    }
    END_KEYMAPPED_VAR(Flags)

    NEW_KEYMAPPED_VAR(ExternalLibs, std::unordered_set<std::string>{})
    { 
      const auto libs = Tokenize(inVal, ',');
      for(const auto& lib : libs)
      {
        value.emplace(My.CompilerFlagToken() + My.CompilerLinkerPrefix() + lib);
      }
    }
    END_KEYMAPPED_VAR(ExternalLibs)
    
    NEW_KEYMAPPED_VAR(Dependencies, std::unordered_set<std::string>{})
    {
      value.emplace(inVal);
    }
    END_KEYMAPPED_VAR(Dependencies)
  END_CONFIG_FILE()
}

