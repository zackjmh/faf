#include <future>

#include <FAFcore/Logger.h>
#include <FAFcore/Process.h>
#include <FAFcore/ParallelFor.h>
#include <FAFbuild/FAFfile.h>
#include <FAFbuild/Archiver.h>

#include <FAFbuild/Static.h>

namespace FAF::Build
{


  StaticLibrary StaticLibrary::Build(const Filesystem::ExistingDirectory& libDir, const FAFfile& configFile)
  {
    Logging::ErrorLog("Entering lib build");
    StaticLibrary newLib{};

    const auto& interfaceExts = configFile.InterfaceExts();

    for(auto& [sourceExt, compiler] : configFile.Compiler())
    {
      const auto interfaceExt = interfaceExts.count(sourceExt) ? interfaceExts.at(sourceExt) : std::string{"NO_EXT"};

      newLib.m_includeFiles = libDir.CollectFilesByExt(interfaceExt);

      using LocatedAtInterfaceDir = Filesystem::File (Filesystem::File::*)(const Filesystem::File&) const;
      
      auto stageHeaders = Async::ParallelDispatcher<std::tuple<Filesystem::File, Filesystem::File>>
      {
        newLib.m_includeFiles,
        Functional::Passthrough(LocatedAtInterfaceDir(&Filesystem::File::LocatedAt), configFile.HeaderPrefixDir())
      }.Then(Functional::AlphaAndOmega(&Filesystem::File::DoIfNewer<decltype(&Filesystem::File::EnsureCopyTo), const Filesystem::File&, const Filesystem::File&>,
                                       &Filesystem::File::EnsureCopyTo));

      newLib.m_archive = configFile.ArchiveFile();

      const Archiver libArchiver{newLib.m_archive};

      const auto sources = libDir.CollectFilesByExt(sourceExt);

      using WithInterfaceExt = Filesystem::File (Filesystem::File::*)(const std::string_view&) const;

      auto compileFiles = Async::ParallelDispatcher<std::tuple<Filesystem::File, Filesystem::File>>
      {
          sources, 
          Functional::Passthrough(WithInterfaceExt(&Filesystem::File::WithExt), interfaceExt)
      };
      
      stageHeaders.Wait("Failed to stage header: ");

      compileFiles.Then(Functional::OnThis(&Compiler::Compile, compiler, configFile, false))
       .Then(Functional::OnThis(&Archiver::Archive, libArchiver))
       .Wait("Object file failed to build: ", Functional::OnThis(&Collections::Vector<Filesystem::File>::EmplaceBack<const Filesystem::File&>, newLib.m_objects));
    }
    return newLib;
  }

  const Filesystem::File& StaticLibrary::Library() const
  {
    return m_archive;
  }
}
