#pragma once
#include <functional>

#include <FAFcore/Directory.h>
#include <FAFcore/File.h>
#include <FAFcore/Vector.h>

#include <FAFbuild/FAFfile.h>
namespace FAF::Build
{

  class StaticLibrary
  {
  public:
    static StaticLibrary Build(const Filesystem::ExistingDirectory& libDir, const FAFfile& configFile);

    const Filesystem::File& Library() const;
    const Collections::Vector<Filesystem::File>& Includes() const
    {
      return m_includeFiles;
    }

    ~StaticLibrary(){};

    StaticLibrary() = default; 

  private:
    Filesystem::File m_archive{};
    Collections::Vector<Filesystem::File> m_objects{};
    Collections::Vector<Filesystem::File> m_includeFiles{};
  };
}
