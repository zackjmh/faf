#include "Installed.h"

namespace FAF::Project
{

  const Filesystem::RequiredDirectory Installed::FAF_BIN{Loaded::FAF_BIN_DIR};
  const Filesystem::RequiredDirectory Installed::FAF_LIB{Loaded::FAF_LIB_DIR};
  const Filesystem::RequiredDirectory Installed::FAF_INCLUDE{Loaded::FAF_INCLUDE_DIR};
  
  Installed::Installed(const Filesystem::File& toInstall)
    : Built{toInstall},
      Loaded{Name()}
  {
    Filesystem::Directory::EnsureExists(Loaded::m_driversDir);
    Filesystem::Directory::EnsureExists(Loaded::m_includeDir);

    auto installAndReferenceFrom = [](const auto& source, const auto& target, const auto& refDir)
    {
      source.EnsureCopyTo(target);
      target.EnsureSymlinkTo(source.LocatedAt(refDir));
    };

    for(auto&& driver : m_drivers.Binaries())
    {
      auto targetDir = driver.LocatedAt(Loaded::m_driversDir.Path());
      Logging::ColorLog("Installing driver %s", Logging::Colors::MAGENTA, driver.Name());

      driver.DoIfNewer(targetDir, installAndReferenceFrom, driver, targetDir, FAF_BIN_DIR);
    }

    auto& configFile = ConfigFile().FAFfile();

    configFile.DoIfNewer(Loaded::m_configFile, &Filesystem::File::EnsureCopyTo, configFile, Loaded::m_configFile);

    Logging::ColorLog("Installing library %s", Logging::Colors::MAGENTA, m_lib.Library().Name());

    for(auto&& include : m_lib.Includes())
    {
      auto targetFile = include.LocatedAt(Loaded::m_includeDir.Path());
      include.DoIfNewer(targetFile, &Filesystem::File::EnsureCopyTo, include, targetFile); 
    }

    auto archiveFile = m_lib.Library();
    
    archiveFile.DoIfNewer(Loaded::m_archive, installAndReferenceFrom, archiveFile, Loaded::m_archive, FAF_LIB_DIR);

    m_includeDir.EnsureSymlinkTo(Filesystem::File{FAF_INCLUDE_DIR + Name()});

    if(!Dependable())
    {
      throw std::runtime_error("Install failed to make project usable as a dependency!");
    }
  }
}
