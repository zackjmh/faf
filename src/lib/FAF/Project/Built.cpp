#include <FAFcore/File.h>
#include <FAFcore/ParallelFor.h>
#include <FAFcore/Vector.h>
#include <FAFbuild/Static.h>
#include <FAFbuild/Binary.h>

#include <FAFbuild/Loaded.h>
#include <FAFbuild/Built.h>
#include <FAFbuild/ProjectFile.h>

namespace FAF::Project
{
  Built::Built(const Filesystem::File&)
    : Workspace{}
  { 
    if(!IsConstructed())
    {
      throw std::runtime_error("Can't build malformed project!");
    }

    const auto& configFile = ConfigFile().FAFfile();

    try
    {
      auto libDir = FAF::Filesystem::ExistingDirectory(m_libsDir.Path());
      FAF::Logging::ColorLog("Starting library %s build!", FAF::Logging::Colors::MAGENTA, Name());
      m_lib = FAF::Build::StaticLibrary::Build(libDir, configFile);
      FAF::Logging::ColorLog("Library built!", FAF::Logging::Colors::GREEN);
    }
    catch(std::exception& e)
    {
      FAF::Logging::ErrorLog("Libraries failed to build");
      throw;
    }

    try
    {
      auto testDir = Filesystem::ExistingDirectory(m_testsDir.Path());
      FAF::Logging::ColorLog("Starting tests build!", FAF::Logging::Colors::GREEN);
      m_tests = FAF::Build::Binary::Build(testDir, {m_lib}, configFile);
     
      FAF::Logging::ColorLog("Tests built! Now running!", FAF::Logging::Colors::GREEN);
      
      RunTests();
        
      FAF::Logging::ColorLog("Tests passed!", FAF::Logging::Colors::GREEN);
    }
    catch(std::exception& e)
    {
      FAF::Logging::ErrorLog("Test phase failed!");

      throw;
    }

    if(!IsDriverless())
    {
      try
      {
        auto binDir = Filesystem::ExistingDirectory(m_driversDir.Path());
        FAF::Logging::ColorLog("Starting binary build!", Logging::Colors::GREEN);
        m_drivers = Build::Binary::Build(binDir, {m_lib}, configFile);
        Logging::ColorLog("Binaries built!", Logging::Colors::GREEN);
      }
      catch(std::exception& e)
      {
        FAF::Logging::ErrorLog("Binaries failed to build!");

        throw;
      }
    }

    FAF::Logging::ColorLog("Build completed!", FAF::Logging::Colors::GREEN);  
  }

  void Built::RunTests()
  {
    Async::ParallelDispatcher<void> RunTests{m_tests.Binaries(), [](auto&& test)
    {
      auto&& testName = test.Name();
      Logging::ColorLog("Running test %s", Logging::Colors::GREEN, testName);
      auto runningTest = System::Process(test.Path());

      if(runningTest.ReturnValue() == 0)
      {
        Logging::ColorLog("Test %s passed!", Logging::Colors::GREEN, testName);
      }
      else
      {
        Logging::ErrorLog("Test %s failed!", testName);
      }
    }};

  }

}
