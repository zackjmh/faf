#pragma once
#include <string>
#include <FAFcore/RequiredDirectory.h>
#include <FAFbuild/Loaded.h>
#include <FAFbuild/Built.h>

namespace FAF::Project
{

  class Installed : public Built, public Loaded
  {
  public: 
    static const Filesystem::RequiredDirectory FAF_BIN;
    static const Filesystem::RequiredDirectory FAF_INCLUDE;
    static const Filesystem::RequiredDirectory FAF_LIB;

    Installed(const Filesystem::File& projectName);
  };
}
