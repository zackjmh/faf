#pragma once
#include <FAFbuild/Binary.h>
#include <FAFbuild/Static.h>
#include <FAFbuild/Workspace.h>

namespace FAF::Project
{
  class Built : public Workspace
  {
  public:
    Built(const Filesystem::File& projectDir);

    std::optional<Filesystem::File> FindDriver(const std::string_view& toFind) const
    {
      std::optional<Filesystem::File> out{};
      for(auto& driver : m_drivers.Binaries())
      {
        if(toFind == driver.Fullname())
        {
          out.emplace(driver);
          break;
        }
      }

      return out;
    }

  protected:
    Build::Binary m_tests{};
    Build::Binary m_drivers{};
    Build::StaticLibrary m_lib;
  
    void RunTests();
  };

}
