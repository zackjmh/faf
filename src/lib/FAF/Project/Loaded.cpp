#include <FAFcore/XDGPaths.h>

#include <FAFbuild/Loaded.h>


namespace FAF::Project
{
  const std::string Loaded::INSTALL_DIR = Filesystem::XDGPaths::Variables().GetDataHome() + INSTALL_BASE_DIR;
  const std::string Loaded::FAF_BIN_DIR = Loaded::INSTALL_DIR + "bin/";
  const std::string Loaded::FAF_LIB_DIR = Loaded::INSTALL_DIR + "lib/";
  const std::string Loaded::FAF_INCLUDE_DIR = Loaded::INSTALL_DIR + "include/"; 

  Loaded::Loaded(const std::string& projectName)
    : m_archive{GetInstalledDir(projectName, "/lib" + projectName + ".a")},
      m_includeDir{GetInstalledDir(projectName, "/include/")},
      m_driversDir{GetInstalledDir(projectName, "/Drivers/")},
      m_configFile{GetInstalledDir(projectName, "/FAFfile")}
  {}

  Loaded::DependencySet Loaded::ResolveDependencies() const
  {
    auto thisDepends = DependenciesFrom(Filesystem::File{"FAFfile"});

    // TODO: Fix this monstrosity
    for(auto& [dependency, configFile] : thisDepends)
    {
      const auto dependDepends = DependenciesFrom(dependency.m_configFile);

      for(auto& furtherDependency : dependDepends)
      {
        if(!thisDepends.count(furtherDependency))
        {
          thisDepends.emplace(std::move(furtherDependency));
        }
      }
    }

    return thisDepends;
  }

  Loaded::DependencySet Loaded::DependenciesFrom(const Filesystem::File& configFile)
  {
    Build::FAFfile toLoad{configFile};

    DependencySet out{};
    const auto startingSet = toLoad.Dependencies();

    for(auto&& project : startingSet)
    {
      const auto loadedProj = Loaded{project};
      const auto projConfig = Build::FAFfile{loadedProj.m_configFile};
      out.emplace(std::move(loadedProj), std::move(projConfig));
    }

    return out;
  }

  Filesystem::File Loaded::GetInstalledDir(const std::string& project, const std::string& dir)
  {
    return Filesystem::File{INSTALL_DIR + project + dir};
  }
}
