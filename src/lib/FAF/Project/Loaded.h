#pragma once
#include <string>
#include <set>
#include <FAFcore/File.h>
#include <FAFbuild/FAFfile.h>
#include <FAFbuild/Compiler.h>
#include <FAFcore/Logger.h>

namespace FAF::Project
{
  class Loaded
  {
  public: 
    
    class Hashor
    {
    public:
      int operator()(const std::pair<Loaded, Build::FAFfile>& toHash) const
      {
        return std::hash<std::string>()(toHash.first.m_archive.Path());
      }
    };
    
    using DependencySet = std::unordered_set<std::pair<Loaded, Build::FAFfile>, Hashor>;
    
    static constexpr auto INSTALL_BASE_DIR = "FAF/";
    static const std::string INSTALL_DIR;
    static const std::string FAF_BIN_DIR;
    static const std::string FAF_LIB_DIR;
    static const std::string FAF_INCLUDE_DIR;

    Loaded(const std::string& loaded);

    DependencySet ResolveDependencies() const;

    bool Dependable() const
    {
      return m_archive.Exists() && m_includeDir.Exists();
    }

    const Filesystem::File& Archive() const
    {
      return m_archive;
    }

    bool operator==(const Loaded& rhs) const
    {
      return m_archive == rhs.m_archive && m_includeDir == rhs.m_includeDir;
    }

    static DependencySet DependenciesFrom(const Filesystem::File& toLoad);

    static Filesystem::File GetInstalledDir(const std::string& project, const std::string& dir = "/");
  protected: 
    DependencySet Dependencies(const Filesystem::File& configFile) const;

    Filesystem::File m_archive;
    Filesystem::File m_includeDir;
    Filesystem::File m_driversDir;
    Filesystem::File m_configFile;
  };
}
