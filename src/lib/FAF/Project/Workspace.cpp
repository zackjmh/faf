#include "Workspace.h"

namespace FAF::Project
{ 
  Workspace Workspace::Create(const Filesystem::File& projectName)
  {
    Workspace newProject{};

    if(newProject.IsConstructed())
    {
      throw std::runtime_error("Project already created!");
    }

    Filesystem::Directory::SetupNew(projectName, [&newProject, project=projectName.Name()]()
    {
      Logging::ColorLog("Setting up source dirs", Logging::Colors::GREEN);

      Filesystem::File::Create(".gitignore", "#FAF default ignores\nbuild/\n");
      Filesystem::File::Create("FAFfile", "projectName=" + project + "\n");
      Filesystem::File::Create("README.md", "# " + project + "\nA FAF generated project\n");
      Filesystem::Directory::WorkIn(newProject.m_driversDir, Filesystem::File::Create, "/README.md", "#Drivers\nAll driver classes will be generated in this dir\n");
      Filesystem::Directory::WorkIn(newProject.m_libsDir, Filesystem::File::Create, "/README.md", "#Lib\nProgram logic classes go here, these will build into static libs\n");
      Filesystem::Directory::WorkIn(newProject.m_testsDir, Filesystem::File::Create, "/README.md", "#Tests\nEvery class needs tests, which will be run before drivers build\n");

      Logging::ColorLog("Creating git repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"init"});

      Logging::ColorLog("Adding project to repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"add", "."});

      Logging::ColorLog("Commiting project to repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"-c","user.name=FAFmanage", "-c", "user.email=FAFmanage@auto.noemail", "commit", "-m\"FAF Init\""});
      if(!newProject.IsConstructed())
      {
        throw std::runtime_error("An unspecified error prevented project construction! Project is left in place as this may be a false positive.");
      }
    });



    return newProject;
  } 

  bool Workspace::IsConstructed()
  {
    return m_libsDir.Exists()
	      && m_testsDir.Exists()
	      && Filesystem::File(".git").Exists();
  }
}
