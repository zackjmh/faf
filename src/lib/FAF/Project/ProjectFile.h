#pragma once
#include <FAFbuild/FAFfile.h>
#include <FAFbuild/Loaded.h>

namespace FAF::Project
{
  CONFIG_FILE(ProjectFile)
    NEW_KEYMAPPED_VAR(FAFfile, Build::FAFfile{})
    { 
      value.Reload(inVal);
    }
    END_KEYMAPPED_WITH_CACHE(FAFfile)
    {
      const auto depends = Loaded(value.ProjectName()).ResolveDependencies(); 

      for(auto& [name, constCompiler] : cache.Compiler())
      {
        auto& compiler = const_cast<Build::Compiler&>(constCompiler);
        for(const auto& [depend, configFile] : depends)
        {
          compiler.AddStaticLib(depend.Archive());

          for(const auto& lib : configFile.ExternalLibs())
          {
            compiler.AddLinkerDepends(lib);
          }
        }

        compiler.AddIncludeDir(Filesystem::File{value.LocalIncludeDir()});
        compiler.AddIncludeDir(Filesystem::File{Loaded::FAF_INCLUDE_DIR});
      }
    }
    END_CACHED_GET(FAFfile)
  END_CONFIG_FILE()
}
