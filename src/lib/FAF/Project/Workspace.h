#pragma once

#include <string>
#include <FAFcore/File.h>
#include <FAFcore/Directory.h>
#include <FAFcore/XDGPaths.h>
#include <FAFbuild/Compiler.h>
#include <FAFbuild/Binary.h>
#include <FAFbuild/ProjectFile.h>
#include <FAFcore/Process.h>
#include <FAFcore/Logger.h>

namespace FAF::Project
{
  class Workspace
  {
  public:
    static constexpr auto LIB_DIR = "src/lib";
    static constexpr auto DRIVER_DIR = "src/bin/Drivers";
    static constexpr auto TESTS_DIR = "src/bin/Tests";

    Workspace()
      : m_workspaceRoot{m_config.FAFfile().ProjectName()}
    {
    };

    ~Workspace(){}

    std::string Name() const
    {
      return m_workspaceRoot.Name();
    }

    static Workspace Create(const Filesystem::File& projectName);

    bool IsDriverless()
    {
      return !m_driversDir.Exists();
    }

    const ProjectFile& ConfigFile() const
    {
      return m_config;
    }

  protected:
    ProjectFile m_config{};
    Filesystem::File m_workspaceRoot;
    Filesystem::File m_libsDir{LIB_DIR};
    Filesystem::File m_driversDir{DRIVER_DIR};
    Filesystem::File m_testsDir{TESTS_DIR};

    bool IsConstructed();
  };
}
