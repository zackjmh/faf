#include <cstring>
#include <string>
#include <FAFcore/Directory.h>
#include <FAFbuild/Workspace.h>
#include <FAFbuild/Installed.h>
#include <FAFcore/Logger.h>

int main(int argc, char* argv[])
{
  if(argc < 2)
  {
    FAF::Logging::ErrorLog("Need a target and argument");
    return -1;
  }

  if(strcmp(argv[1], "init") == 0)
  {
    try
    {
      FAF::Logging::ColorLog("Creating project %s", FAF::Logging::Colors::GREEN, argv[2]);
      FAF::Project::Workspace::Create(FAF::Filesystem::File{std::string{argv[2]}});
    }
    catch(std::exception& e)
    {
      FAF::Logging::ErrorLog(e.what());
      return -2;
    }
  }
  else if(strcmp(argv[1], "install") == 0)
  {
    try
    {
      auto project = FAF::Filesystem::Directory::WorkingDir();
      FAF::Logging::ColorLog("Starting install for project %s", FAF::Logging::Colors::GREEN, project);
      FAF::Project::Installed{FAF::Filesystem::File{project}};
    }
    catch(std::exception& e)
    {
      FAF::Logging::ErrorLog(e.what());
      return -3;
    }
  }
}
