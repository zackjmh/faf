//TODO: Prune these
#include <FAFcore/Logger.h>
#include <FAFcore/Directory.h>
#include <FAFbuild/Static.h>
#include <FAFbuild/Binary.h>
#include <FAFbuild/Compiler.h>
#include <FAFcore/Process.h>
#include <FAFbuild/Built.h>

#include <cstring>
#include <iostream>

int main(int argc, const char** argv)
{
  if(argc > 1)
  {
    if(strcmp(argv[1], "bootstrap") == 0 || strcmp(argv[1], "rebuild") == 0)
    {
      std::filesystem::remove_all("build");
    }
    else if(strcmp(argv[1], "clean") == 0)
    {
      FAF::Logging::ColorLog("Build dir cleaned!", FAF::Logging::Colors::GREEN);
      std::filesystem::remove_all("build");
      return 0;
    }
    else
    {
      FAF::Logging::ErrorLog("Error: argument %s not recognized", argv[1]);
      return -10;
    }
  }
  
  try
  {
    const auto projectDir = argc > 2 ? std::string{argv[2]} : FAF::Filesystem::Directory::WorkingDir();
    const auto built = FAF::Project::Built{FAF::Filesystem::File{projectDir}};

    if(argc > 1 && strcmp(argv[1], "bootstrap") == 0)
    {
      try
      {
        FAF::Logging::ColorLog("Starting Bootstrap!", FAF::Logging::Colors::CYAN);

        const auto maybeFafBuild = built.FindDriver("FAFbuild");

        if(!maybeFafBuild)
        {
          throw std::runtime_error("Couldn't find FAFbuild!");
        }

        const auto fafBuild = *maybeFafBuild;

        FAF::System::Process::CreateAndWait(fafBuild.Path(), {"rebuild"});

        FAF::Logging::ColorLog("Bootstrapping successful!", FAF::Logging::Colors::GREEN);

      }
      catch(std::exception& e)
      {
        FAF::Logging::ErrorLog("Bootstrap falied: %s", e.what());

        return -14;
      }
    }
  }
  catch(std::exception& e)
  {
    FAF::Logging::ErrorLog(e.what());

    return -20;
  }
  FAF::Logging::ColorLog("Build Successful!", FAF::Logging::Colors::GREEN);

  return 0;
}
 
