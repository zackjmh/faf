# FAFbuild
FAFbuild is the automated build-system portion of the FAF framework.
If a project conforms to the FAF project structure, FAFbuild can automatically
build and link the project without any configuration files.
